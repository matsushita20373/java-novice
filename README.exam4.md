# 問題4: Charsetの変換にともなう文字化け

Shift_JISにはいくつかの種類があり、相互変換する際に文字化けを引き起こすものが存在します。
Windowsで使われているキャラクターセットであるMS932と、IBM拡張のキャラクターセットであるCp943cの相互変換時に、文字化けを起こす文字を全て返す、SJISVariation#findUnMatchCharsを実装しましょう。
https://ja.wikipedia.org/wiki/Shift_JIS

* 変換時の文字化けの問題は、同じ文字がUnicode上で違うコードポイントに変換されるのが原因です。
* Shift_JISの文字範囲(2バイト文字)は、第1バイトに0x81-0x9Fまたは0xE0-0xEFの47通り、第2バイトに0x40-0x7Eまたは0x80-0xFCの188通りを用いる。これを探索の範囲としてください。